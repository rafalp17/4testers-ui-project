import time
import pytest
from selenium.webdriver import Chrome
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from arena.login_page import LoginPage
from arena.project_page import ProjectPage

administrator_email = 'administrator@testarena.pl'

@pytest.fixture()
def browser():
    service = Service(ChromeDriverManager().install())
    browser = Chrome(service=service)
    browser.set_window_size(1920, 1080)
    login_page = LoginPage(browser)
    login_page.go_to_testarena()
    login_page.login_to_testarena(administrator_email, 'sumXQQ72$L')
    yield browser
    time.sleep(4)
    browser.quit()

@pytest.fixture()
def project_page(browser):
    project_page = ProjectPage(browser)
    project_page.add_new_project()

    yield browser


