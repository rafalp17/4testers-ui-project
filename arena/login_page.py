from selenium.webdriver.common.by import By


class LoginPage:
    def __init__(self, browser):
        self.browser = browser

    def go_to_testarena(self):
        self.browser.get('http://demo.testarena.pl/zaloguj')

    def login_to_testarena(self, email, password):
        self.browser.find_element(By.CSS_SELECTOR, '#email').send_keys(email)
        self.browser.find_element(By.CSS_SELECTOR, '#password').send_keys(password)
        self.browser.find_element(By.CSS_SELECTOR, "#login").click()

