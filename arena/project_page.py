
from selenium.webdriver.common.by import By
from arena.random_name_generator import generate_name


class ProjectPage:

    def __init__(self, browser):
        self.browser = browser
    def add_new_project(self):
        self.browser.find_element(By.CSS_SELECTOR, '.icon_tools').click()
        self.browser.find_element(By.CSS_SELECTOR, '.button_link_li').click()
        self.browser.find_element(By.CSS_SELECTOR, '#name').send_keys(generate_name())
        self.browser.find_element(By.CSS_SELECTOR, '#prefix').send_keys(generate_name())
        self.browser.find_element(By.CSS_SELECTOR, '#description').send_keys('Cwiczenie_zaliczeniowe_4_testers')
        self.browser.find_element(By.CSS_SELECTOR, "#save").click()
        self.browser.find_element(By.CSS_SELECTOR, ".item2").click()
        self.browser.find_element(By.CSS_SELECTOR, '#search').send_keys(self.browser.find_element(By.CSS_SELECTOR, 'td:nth-child(1)').text)
        self.browser.find_element(By.ID, "j_searchButton").click()
