import random
import string

def generate_name(length=5):
    return ''.join(random.choices(string.ascii_uppercase + string.digits, k=length))
