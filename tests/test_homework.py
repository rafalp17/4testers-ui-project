from selenium.webdriver.common.by import By
from arena.fixture import browser, project_page
from arena.home_page import HomePage


def test_successfully_login(browser):
    arena_home_page = HomePage(browser)
    arena_home_page.click_login()
    assert browser.current_url == 'http://demo.testarena.pl/zaloguj'

def test_if_page_is_loaded_succesfully(browser):
    assert browser.find_element(By.CSS_SELECTOR, '.user-info small').text == 'administrator@testarena.pl'


def test_open_projects_page(browser):
    browser.find_element(By.CSS_SELECTOR, '.icon_tools').click()
    assert browser.find_element(By.CSS_SELECTOR, '.content_title').text == 'Projekty'


def test_for_adding_a_new_project(project_page):
    element = project_page.find_element(By.CSS_SELECTOR, 'td:nth-child(1)')
    assert element.is_displayed()



